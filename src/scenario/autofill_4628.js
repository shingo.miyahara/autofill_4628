const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
const fs = require('fs');

const userId = '';
const password = '';

(async() => {
    const browser = await puppeteer.launch({
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox'
        ]
    });
    const page = await browser.newPage();
    
    // TOP
    await page.goto('https://www.4628.jp/needswell/');

    //フォーム入力
    await page.type('input[name="y_logincd"]', userId);
    await page.type('input[name="password"]', password);

    //ログイン
    await page.click('#id_passlogin');

    await page.waitFor(3000);
    
    //出勤簿
    await page.goto('https://www.4628.jp/needswell/?module=timesheet&action=browse');

    await page.waitFor(3000);

    // 編集ボタン押下
    const editbtn = await page.$('#browse_td_edit_2018411 > input[type="image"]');
    await editbtn.click();

    await page.waitFor(3000);

    //勤務条件
    await page.select('select[name="working_conditions_item"]', "52");
    await page.waitFor(3000);

    //出社
    await page.select('select[name="yyyymmdd1"]', "20180411");
    await page.waitFor(3000);

    await page.select('select[name="hour1"]', "9");
    await page.waitFor(3000);

    await page.select('select[name="minute1"]', "30");
    await page.waitFor(3000);

    //退社
    await page.select('select[name="yyyymmdd2"]', "20180411");
    await page.waitFor(3000);

    await page.select('select[name="hour2"]', "19");
    await page.waitFor(3000);

    await page.select('select[name="minute2"]', "0");
    await page.waitFor(3000);

    // プロジェクトコード
    await page.type('input[name="textbox32"]', '100418');

    await page.select('select[name="hour35"]', "8");
    await page.waitFor(3000);

    await page.select('select[name="minute35"]', "0");
    await page.waitFor(3000);

    await page.select('select[name="hour38"]', "0");
    await page.waitFor(3000);

    await page.select('select[name="minute38"]', "30");

    // 保存ボタン押下
    const submit = await page.$('#btn_preserve > input[type="image"]');
    await submit.click();
    
    await page.waitFor(3000);

    //スクショ
    await page.screenshot({path: '/home/pupeeter/src/data/scsho.png', fullPage: true});

    browser.close();
})();
