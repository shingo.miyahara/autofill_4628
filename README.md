# Usage
## build_containar
```
cd docker
docker-compose up -d --build
```
## setup_pupeeter
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash 
source ~/.nvm/nvm.sh 
nvm install v8.4.0 
npm i puppeteer
node -v
npm -v
```
## edit scenario
```
const userId = 'your user id'
const password = 'your password'
```
## execute
```
node /home/pupeeter/src/scenario/autofill_4628.js
```
